package database

import (
	"Toko/controllers"
	"Toko/services"
	"context"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

var (
	userservice    services.Kontrak
	UserController controllers.ControllerToko
	ctx            context.Context
	usercollection *mongo.Collection
	mongoclient    *mongo.Client
)

func Db() {
	ctx = context.TODO()

	mongoconn := options.Client().ApplyURI("mongodb+srv://risuii:babehlo123@cluster.lnue8.mongodb.net/store?retryWrites=true&w=majority")
	mongoclient, err := mongo.Connect(ctx, mongoconn)
	if err != nil {
		log.Fatal(err)
	}
	err = mongoclient.Ping(ctx, readpref.Primary())
	if err != nil {
		log.Fatal(err)
	}

	defer mongoclient.Disconnect(ctx)

	usercollection = mongoclient.Database("userdb").Collection("users")
	userservice = services.CallHandlerToko(usercollection, ctx)
	UserController = controllers.New(userservice)
}
